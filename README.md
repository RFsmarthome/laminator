# Laminator
This project is used for modifing a laminator to use with the toner transfer method for building
own PCBs.

The controller use a LCD display for showing the current voltage at the temperature sensor. You
can also control with is the the destination temp. It use a solid state relais to control the
heating elements and to isolate the control electric from the AC power.

Simple use: You can turn the incremental encoder to adjust the target volatge. By pressing
the button on the incremental encoder you can switch the heating on or off. This is useful
for cooling down the heating elements.

Here some pictures of the lamintator and from a PCB.

The used laminator was a Olympia A296 Plus

[Laminator](images/laminator02.jpg)
[Laminator](images/laminator01.jpg)

The module in the center is a ESP32.

[PCB](images/pcb01.jpg)
