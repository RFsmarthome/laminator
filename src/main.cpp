#include <Arduino.h>
#include <avr/power.h>

#include <Ticker.h>
#include <TimerOne.h>

#include <Wire.h>
#include <LiquidCrystal.h>
#include <FastLED.h>
#include <ClickEncoder.h>

const int analogPin = A0;
const int heatingPin = A1;
const int powerLedPin = 7;

#define ROT_A A2
#define ROT_B A3
#define ROT_BUTTON A4


int a = 0;

void cbHeating();
void cbDisplayLiquid();
void timerIsr();

Ticker tickerHeating(cbHeating, 1000);
Ticker tickerDisplayLiquid(cbDisplayLiquid, 200);

#define V 4.0f
#define VMIN 0.2f
int sollAnalogRead = 1024.0f*V/5.0f;
const int minAnalogRead = 1024.0f*VMIN/5.0f;
int valueAnalogRead = 0;

bool error = false;
bool enableHeating = false;
bool powerOn = true;

ClickEncoder *clickEncoder;
LiquidCrystal liquidCrystal(8, 9, 0, 1, 2, 3, 4, 5, 6, 7);
CRGBArray<1> leds;

/*
 1,995V -> 21.5 (408)
 2,744V -> 200 (560)
*/

void setup() {
  // Switch to 8Mhz
	clock_prescale_set(clock_div_1);
  error = false;

  liquidCrystal.begin(16, 2);
  liquidCrystal.clear();
  liquidCrystal.print("Init...");

  pinMode(heatingPin, OUTPUT);
  digitalWrite(heatingPin, LOW);
  pinMode(powerLedPin, OUTPUT);

  clickEncoder = new ClickEncoder(ROT_B, ROT_A, ROT_BUTTON, 1);



	FastLED.addLeds<WS2812B,powerLedPin, EOrder::GRB>(leds, 1).setCorrection(TypicalLEDStrip);
	FastLED.setBrightness(100);
  leds.fill_solid(CRGB::Green);
  FastLED.show();
  delay(200);
  leds.fill_solid(CRGB::White);
  FastLED.show();
  delay(200);
  leds.fill_solid(CRGB::Green);
  FastLED.show();
  delay(200);

  Timer1.initialize(1000);
  Timer1.attachInterrupt(timerIsr);

  tickerHeating.start();
  tickerDisplayLiquid.start();
}

void loop() {
  valueAnalogRead = analogRead(analogPin);

  sollAnalogRead += clickEncoder->getValue();
  if(sollAnalogRead<0) {
    sollAnalogRead = 0;
  } else if(sollAnalogRead>1023) {
    sollAnalogRead = 1023;
  }

  ClickEncoder::Button buttonStatus = clickEncoder->getButton();
  if(buttonStatus != ClickEncoder::Open) {
    switch(buttonStatus) {
      case ClickEncoder::Clicked:
        powerOn = !powerOn;
        break;
      case ClickEncoder::DoubleClicked:
        error = false;
        break;
      case ClickEncoder::Open:
      case ClickEncoder::Closed:
      case ClickEncoder::Pressed:
      case ClickEncoder::Held:
      case ClickEncoder::Released:
        break;
    }
  }

  tickerHeating.update();
  tickerDisplayLiquid.update();
}

void timerIsr() {
  clickEncoder->service();
}

void cbDisplayLiquid() {
  String line1;
  String line2;
  line1 = "Curr(V): "+ String(((float)valueAnalogRead)*5.0f/1024.0f, 2) +"V";
  line2 = "Soll(V): "+ String(((float)sollAnalogRead)*5.0f/1024.0f, 2) +"V";

  liquidCrystal.setCursor(0, 0);
  liquidCrystal.print(line2);

  liquidCrystal.setCursor(0, 1);
  liquidCrystal.print(line1);
  liquidCrystal.setCursor(15, 1);
  if(error) {
    liquidCrystal.print("E");
  } else {
    if(enableHeating) {
      liquidCrystal.print("H");
    } else {
      liquidCrystal.print("C");
    }
  }

  liquidCrystal.setCursor(15, 0);
  if(powerOn) {
    liquidCrystal.print("*");
  } else {
    liquidCrystal.print(" ");
  }
}

void cbHeating() {
  if(error || !powerOn) {
    // Set again, safety first... Bur not realy needed
    enableHeating = false;
    leds.fill_solid(CRGB::Red);
  } else {
    if(valueAnalogRead > sollAnalogRead) {
      // Hot enought
      enableHeating = false;
      leds.fill_solid(CRGB::Green);
    } else if(valueAnalogRead < minAnalogRead) {
      // Too cold, maybe defect NTC
      error = true;
      enableHeating = false;
      leds.fill_solid(CRGB::Red);
    } else {
      // Not hot enough, heating on
      enableHeating = true;
      leds.fill_solid(CRGB::Yellow);
    }
  }
  digitalWrite(heatingPin, enableHeating?HIGH:LOW);
  FastLED.show();
}
